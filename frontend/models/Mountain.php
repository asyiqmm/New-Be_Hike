<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mountain".
 *
 * @property integer $Id_location
 * @property string $Nama_Gunung
 * @property string $Lokasi_Gunung
 * @property integer $DPL
 */
class Mountain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mountain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama_Gunung', 'Lokasi_Gunung', 'DPL'], 'required'],
            [['DPL'], 'integer'],
            [['Nama_Gunung', 'Lokasi_Gunung'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Id_location' => Yii::t('app', 'Id Location'),
            'Nama_Gunung' => Yii::t('app', 'Nama  Gunung'),
            'Lokasi_Gunung' => Yii::t('app', 'Lokasi  Gunung'),
            'DPL' => Yii::t('app', 'Dpl'),
        ];
    }
}
