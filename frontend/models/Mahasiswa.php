<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property integer $NRP
 * @property string $Nama
 * @property integer $Semester
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nama', 'Semester'], 'required'],
            [['Semester'], 'integer'],
            [['Nama'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NRP' => 'Nrp',
            'Nama' => 'Nama',
            'Semester' => 'Semester',
        ];
    }
}
